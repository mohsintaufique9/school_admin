<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*============== Start Admin Authentication Route List =========================*/

Route::get('/', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin', 'Admin\LoginController@login')->name('admin.login.post');
Route::post('admin', 'Admin\LoginController@login')->name('admin.login.post');
Route::get('admin-logout', 'Admin\LoginController@logout')->name('admin.logout');

Auth::routes();

Route::middleware(['demo'])->group(function(){

    /*============== Admin Password Reset Route list ===========================*/
    
    Route::get('admin-password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('admin-password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('admin-password/reset', 'Admin\ResetPasswordController@reset');
    
    /*===================== Admin Dashboard Redirected ====================*/
    
    Route::get('admin-dashboard',['as'=>'dashboard','uses'=>'DashboardController@getDashboard']);
    
    /*============= Admin Password Change =====================*/
    
    Route::get('admin-change-password', ['as'=>'admin-change-password', 'uses'=>'BasicSettingController@getChangePass']);
    Route::post('admin-change-password', ['as'=>'admin-change-password', 'uses'=>'BasicSettingController@postChangePass']);
    
    /*============ Basic Setting Controller ========================*/
    
});


Route::group([
    'prefix' => 'school_sites',
], function () {
    Route::get('/', 'SchoolSitesController@index')
         ->name('school_sites.school_site.index');
    Route::get('/create','SchoolSitesController@create')
         ->name('school_sites.school_site.create');
    Route::get('/show/{schoolSite}','SchoolSitesController@show')
         ->name('school_sites.school_site.show');
    Route::get('/{schoolSite}/edit','SchoolSitesController@edit')
         ->name('school_sites.school_site.edit');
    Route::post('/', 'SchoolSitesController@store')
         ->name('school_sites.school_site.store');
    Route::put('school_site/{schoolSite}', 'SchoolSitesController@update')
         ->name('school_sites.school_site.update');
    Route::delete('/school_site/{schoolSite}','SchoolSitesController@destroy')
         ->name('school_sites.school_site.destroy');
});

?>