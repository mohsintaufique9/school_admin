<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\SchoolSite;
use Illuminate\Http\Request;
use Exception;

class SchoolSitesController extends Controller
{

    /**
     * Display a listing of the school sites.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $data['page_title'] = 'Schools';
        $data['schoolSites'] = SchoolSite::paginate(25);

        return view('school_sites.index', $data);
    }

    /**
     * Show the form for creating a new school site.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        $data['page_title'] = 'Add School';
        return view('school_sites.create', $data);
    }

    /**
     * Store a new school site in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            SchoolSite::create($data);

            return redirect()->route('school_sites.school_site.index')
                ->with('success_message', 'School Site was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified school site.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $data['page_title'] = 'View School' ;
        $data['schoolSite'] = SchoolSite::findOrFail($id);

        return view('school_sites.show', $data);
    }

    /**
     * Show the form for editing the specified school site.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $data['page_title'] = 'Edit School' ;
        $data['schoolSite'] = SchoolSite::findOrFail($id);

        return view('school_sites.edit', $data);
    }

    /**
     * Update the specified school site in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $schoolSite = SchoolSite::findOrFail($id);
            $schoolSite->update($data);

            return redirect()->route('school_sites.school_site.index')
                ->with('success_message', 'School Site was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified school site from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $schoolSite = SchoolSite::findOrFail($id);
            $schoolSite->delete();

            return redirect()->route('school_sites.school_site.index')
                ->with('success_message', 'School Site was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'name' => 'required|string|min:1|max:191',
            'url' => 'required|string|min:1|max:191', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
