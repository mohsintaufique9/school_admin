<?php

namespace App\Http\Controllers\Auth;

use App\BasicSetting;
use App\Bonus;
use App\TraitsFolder\MailTrait;
use App\User;
use App\Http\Controllers\Controller;
use App\UserLog;
use App\ReferralUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
class RegisterController extends Controller
{
    use MailTrait;
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/virtualoffice/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $basic = BasicSetting::first();
        if ($basic->google_recap == 1){
            Config::set('captcha.secret', $basic->google_secret_key);
            Config::set('captcha.sitekey', $basic->google_site_key);
        }
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {

        $basic = BasicSetting::first();
        if ($basic->user_reg == 0){
            session()->flash('message','Registration Is Currently Deactivate. Please Try Letter.');
            session()->flash('type','danger');
            return redirect()->route('login');
        }
        
        $data['page_title'] = "Register";
        $data['reference'] = '0';
        $data['name'] = session('soname', '');
        $data['email'] = session('soemail', '');
        return view('auth.register',$data);
    }

    function getNode($referral) {

        $result = $this->checkChildren($referral);

        if($result == false) {

            foreach($referral->getImmediateDescendants() as $children) {

                $check = $this->checkChildren($children);
              
                if($check !== false) {
                    break;
                }
            }
            return $check;
        }
        else {
            return $result;
        }
    }

    function checkChildren($referral) {

        $result = false;

        if($referral->getImmediateDescendants()->count() < 2) {

            return $referral;
        }

        foreach($referral->getImmediateDescendants() as $children) {

            if($children->getImmediateDescendants()->count() < 2) {
                $result = $children;
            break;
            }
        }
        return $result;
    }

    public function showReferenceLoginForm($id)
    {
       $basic = BasicSetting::first();
        if ($basic->user_reg == 0){
            session()->flash('message','Registration Is Currently Deactivate. Please Try Letter.');
            session()->flash('type','danger');
            return redirect()->route('login');
        }

        $data['page_title'] = "Register";
        $data['reference'] = $id;
        return view('auth.register',$data);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'first_phone' => 'required|numeric|min:10|unique:users',
            'username' => 'required|min:5|unique:users|regex:/^\S*$/u',
            'password' => 'required|string|min:6|confirmed',
            'g-recaptcha-response' => 'captcha',
            'cnic' => 'required|unique:users',
            'address_1' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data, $ip)
    {

        $user = new User();
        
        if($data['refid'] !==null) {

            $referrer= $user->select('id')->where('username','=',$data['refid'])->first();
            $pid = $referrer->id;

            $referral = ReferralUser::whereUserId($pid)->first();
        }

        $basic = BasicSetting::first();
        $reference = 0;

        if ($basic->email_verify == 1){
            $email_verify = 0;
        }else{
            $email_verify = 1;
        }

        if ($basic->phone_verify == 1){
            $phone_verify = 0;
        } else{
            $phone_verify = 1;
        }

        $email_code = strtoupper(Str::random(6));
        $email_time = Carbon::parse()->addMinutes(5);
        $phone_code = strtoupper(Str::random(6));
        $phone_time = Carbon::parse()->addMinutes(5);

        $new_user = User::create([
            'refid' => $pid ?? null,
            'posid' => $pid ?? 1,
            'package_id' => 0,
            'position' => $data['position'],
            'email' => $data['email'],
            'docid'=> 0,
            'DOB'=> $data['DOB'],
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'last_name' => $data['last_name'],
            'username' => $data['username'],
            'reference' => $data['refid'] ?? 1,
            'address_1' => $data['address_1'],
            'address_2' => $data['address_2'],
            'address_3' => $data['address_3'],
            'first_phone' => $data['first_phone'],
            'second_phone' => $data['second_phone'],
            'cnic' => $data['cnic'],
            'ip_address' => $ip,
            'is_invest' => 0,
            'shares' => 0,
            'id_image' => 0,
            'network' => 0,
            'awallet' => 0,
            'total'=> 0,
            'direct' => 0,
            'deposits' => 0,
            'total_points' => 0,
            'under_reference' => $reference,
            'email_verify' => $email_verify,
            'email_code' => $email_code,
            'email_time' => $email_time,
            'phone_verify' => $phone_verify,
            'phone_code' => $phone_code,
            'phone_time' => $phone_time,
            'password' => bcrypt($data['password']),
        ]);

        $user = User::orderBy('id', 'DESC')->first();

        $user = ReferralUser::create(['user_id' => $user->id]);
        
        if($referrer) {

            $referral = ReferralUser::whereUserId($referrer->id)->first();

            $result = $this->getNode($referral);

            $user->makeChildOf($result);
        }

        return $new_user;
    }

    protected function registered(Request $request, $user)
    {
        $basic = BasicSetting::first();

        if ($user->reference != 0){
            $refUser = User::findOrFail($user->reference);

            $bal4 = $refUser;

            if ($basic->email_notify == 1){
                $text = $user->name."has joined Aurya Global";
                $this->sendMail($bal4->email,$bal4->name,'New Referal',$text);
            }
            if ($basic->phone_notify == 1){
                $text = $user->name." has joined Aurya Global";
                $this->sendSms($bal4->phone,$text);
            }
        }


        if ($basic->email_verify == 1)
        {
            $email_code = strtoupper(Str::random(6));
            $text = "Your Verification Code Is: <b>$email_code</b>";
            $this->sendMail($user->email,$user->name,'Email verification',$text);
            $user->email_code = $email_code;
            $user->email_time = Carbon::parse()->addMinutes(5);
            $user->save();
        }
        if ($basic->phone_verify == 1)
        {
            $email_code = strtoupper(Str::random(6));
            $txt = "Your Verification Code is: $email_code";
            $to = $user->phone;
            $this->sendSms($to,$txt);
            $user->phone_code = $email_code;
            $user->phone_time = Carbon::parse()->addMinutes(5);
            $user->save();
        }
    }
}
