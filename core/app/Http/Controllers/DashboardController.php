<?php

namespace App\Http\Controllers;

use App\BasicSetting;
use App\TraitsFolder\MailTrait;
use App\SchoolSite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Exception\RequestException;

class DashboardController extends Controller
{
    use MailTrait;
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->guzzle = app('guzzle');
    }
    public function getDashboard()
    {
        $result=[];

        $school_sites = SchoolSite::get();

        foreach($school_sites as $site) {
            try {
                $response = $this->guzzle->request('get', $site->url . '/api');
            
                if($response->getStatusCode() == 200) {

                    $response_body = json_decode($response->getBody());
                    if ($response_body->status == true) {

                        $result[] = $response_body;
                    }
                }
            }
          
            catch(RequestException  $e) {
                
            }
        }

        $schools = collect($result);
        
        $data['total_users'] =  $schools->sum(function ($school) {
            return count($school->users);
        });

        $data['total_teachers'] =  $schools->sum(function ($school) {
            return count($school->teachers);
        });

        $data['total_schools'] =  $schools->count();

        $data['total_students'] =  $schools->sum(function ($school) {
            return count($school->students);
        });

        $data['page_title'] = "Dashboard";
        // dd('reached');

        return view('dashboard.dashboard', $data);
    }
}
