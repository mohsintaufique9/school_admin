<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $site_title }} | {{ $page_title }}</title>
    <!--Favicon add-->
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/images/logo/icon.png') }}">
    <!--bootstrap Css-->
    <link href="{{ asset('assets/front/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--font-awesome Css-->
    <link href="{{ asset('assets/front/css/font-awesome.min.css') }}" rel="stylesheet">
    <!--Style Css-->
    <link href="{{ asset('assets/front/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/mymain.css') }}" rel="stylesheet">

    <!-- Mymain css -->
    <!--Responsive Css-->
    <link href="{{ asset('assets/front/css/responsive.css') }}" rel="stylesheet">  
</head>
    <style>
        .btn-facebook {
            color: #ffffff !important;
            text-decoration: none !important;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #2b4b90;
            *background-color: #133783;
            background-image: -moz-linear-gradient(top, #3b5998, #133783);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#3b5998), to(#133783));
            background-image: -webkit-linear-gradient(top, #3b5998, #133783);
            background-image: -o-linear-gradient(top, #3b5998, #133783);
            background-image: linear-gradient(to bottom, #3b5998, #133783);
            background-repeat: repeat-x;
            border-color: #133783 #133783 #091b40;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff3b5998', endColorstr='#ff133783', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        }

        .btn-facebook:hover,
        .btn-facebook:focus,
        .btn-facebook:active,
        .btn-facebook.active,
        .btn-facebook.disabled,
        .btn-facebook[disabled] {
            color: #ffffff !important;
            background-color: #133783;
            *background-color: #102e6d;
        }

        .btn-facebook:active,
        .btn-facebook.active {
            background-color: #0d2456 \9;
        }
        .btn-google-plus {
            color: #ffffff !important;
            text-decoration: none !important;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
            background-color: #d34332;
            *background-color: #c53727;
            background-image: -moz-linear-gradient(top, #dd4b39, #c53727);
            background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#dd4b39), to(#c53727));
            background-image: -webkit-linear-gradient(top, #dd4b39, #c53727);
            background-image: -o-linear-gradient(top, #dd4b39, #c53727);
            background-image: linear-gradient(to bottom, #dd4b39, #c53727);
            background-repeat: repeat-x;
            border-color: #c53727 #c53727 #85251a;
            border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffdd4b39', endColorstr='#ffc53727', GradientType=0);
            filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
        }

        .btn-google-plus:hover,
        .btn-google-plus:focus,
        .btn-google-plus:active,
        .btn-google-plus.active,
        .btn-google-plus.disabled,
        .btn-google-plus[disabled] {
            color: #ffffff !important;
            background-color: #c53727;
            *background-color: #b03123;
        }

        .btn-google-plus:active,
        .btn-google-plus.active {
            background-color: #9a2b1f \9;
        }
    </style>



<!--login section start-->
<section  class="bg">
    <div class="container" >
        <div class="row" >
  
            <div class="col-md-12 my-login">
                    <div class="head-login text-center">
                      <a href="{{url('/')}}"><img src="{{asset('assets/images/logo/logo.png')}}" style="max-height:80px;"></a>
                    </div>
                        @if($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!!  $error !!}
                                </div>
                            @endforeach
                        @endif
                        
                        @if (session()->has('message'))
                            <div class="alert alert-{{ session()->get('type') }} alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        
                        @if (session()->has('status'))
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session()->get('status') }}
                            </div>
                        @endif
                       <div class="login-form"> 
                        <form class="text-left form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <label>Username</label>
                                    <input style="margin-top: 15px;" type="text" name="username" id="username" required/>
                                </div>
                                <div class="col-md-12">
                                    <label>Password</label>
                                    <input type="password" name="password" id="password" required/>
                                </div>         
                            
                            </div>
                            <div class="col-md-12">
                                @if($basic->google_recap == 1)
                                        <div class="col-sm-12" style="margin-top: 10px;">
                                            {!! app('captcha')->display() !!}
                                        </div>
                                @endif
                            </div>
                            <div class="col-md-12 text-center">
                                <input  value="Login" type="submit">
                            </div>    
                           

                           @if($basic->fb_login == '1' || $basic->g_login == '1')

                            <div class="form-group">
                                @if($basic->fb_login == '1')
                                <div class="col-sm-6">
                                    <a href="{{ url('login/facebook') }}" class="btn btn-facebook btn-block">
                                        <i class="fa fa-facebook"></i> | Facebook Login
                                    </a>
                                </div>
                                @endif
                                @if($basic->g_login == '1')
                                <div class="col-sm-6">
                                    <a href="{{ url('login/google') }}" class="btn btn-google-plus btn-block">
                                        <i class="fa fa-google-plus"></i> | Google Login
                                    </a>
                                </div>
                                @endif
                            </div>

                            @endif
                             
                            <div class="col-sm-12 text-center">
                                <p style="color: #FFF">Don't have account? <a style="text-decoration: underline;" href="{{url('register')}}">Create here</a></p>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>

                        </form>
                        </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('assets/js/jquery-1.12.4.min.js') }}"></script> 
<!-- Latest compiled and minified Bootstrap --> 
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script> 
<script type="text/javascript">
    $("form").submit(function(event) {

   var recaptcha = $("#g-recaptcha-response").val();
   if (recaptcha === "") {
      event.preventDefault();
      swal("Please check the recaptcha");
   }
});
</script>