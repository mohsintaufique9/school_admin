<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $site_title }} | {{ $page_title }}</title>
    <!--Favicon add-->
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/images/logo/icon.png') }}">
    <!--bootstrap Css-->
    <link href="{{ asset('assets/front/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--font-awesome Css-->
    <link href="{{ asset('assets/front/css/font-awesome.min.css') }}" rel="stylesheet">
    <!--Style Css-->
    <link href="{{ asset('assets/front/css/style.css') }}" rel="stylesheet">

    <!-- Mymain css -->
    <!--Responsive Css-->
    <link href="{{ asset('assets/front/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/mymain.css') }}" rel="stylesheet">
    
</head>
<style>
    select:required:invalid {
  color: gray;
}
option[value=""][disabled] {
  display: none;
}
option {
  background-color: #fff !important;
  color: black;
}
input{
  -webkit-user-select: initial !important;
  -khtml-user-select: initial !important;
  -moz-user-select: initial !important;
  -ms-user-select: initial !important;
  user-select: initial !important;
}
</style>
 <!--header section start-->
    <!--Header section end-->

    <!--login section start-->
    <section class="reglogin-bg">
        <div class="container">
             <div class="row">
                <div class="head-login text-left">
                            <a href="{{url('/')}}"><img src="{{asset('assets/images/logo/logo.png')}}" style="max-height:80px;"></a>
                         </div>
                 <div class="col-md-12 reg-login" > 
                            @if($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        {!!  $error !!}
                                    </div>
                                @endforeach
                            @endif
                            <form method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}

                                <div class="row">

                                    <div class="col-md-4">
                                        <label>First name</label>
                                        <input type="text" value="{{ $name }}"  name="first_name" id="first_name" required/>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Middle name</label>
                                        <input type="text" value="{{ $name }}"  name="middle_name" id="middle_name" required/>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Last name</label>
                                        <input type="text" value="{{ $name }}"  name="last_name" id="last_name" required/>
                                    </div>

                                    <div class="col-md-12">
                                        <label>Email</label>
                                        <input type="email" value="{{ $email }}"  name="email" id="email" required />
                                    </div>
                                    
                                    <div class="col-xs-12 col-md-6">
                                        <label>Sponser Username</label>
                                        <input required type="text" value="{{ app('request')->input('refid') }}" name="refid" id="refid" readonly />
                                    </div>

                                    <div class="col-xs-12 col-md-6">
                                        <input type="hidden" value="{{ app('request')->input('p') }}" name="position" id="position" required/>
                                    </div>

                                    <!-- <span id="error_email"></span> -->
                                    <div class="col-xs-12 col-md-6">
                                        <label>Username</label>
                                        <input type="text"  name="username" id="username" required/>
                                    </div> 

                                    <div class="col-xs-12 col-md-6">
                                        <label>Date Of Birth</label>
                                        <input type="date"  name="DOB" id="DOB" required placeholder="YYYY/MM/DD" />
                                    </div>
                                    
                                    <div class="col-xs-12 col-md-6">
                                        <label>CNIC</label>
                                        <input type="text" name="cnic" data-inputmask="'mask': '99999-9999999-9'"  placeholder="XXXXX-XXXXXXX-X" id="username" required/>
                                    </div>  

                                    <div class="col-xs-12 col-md-12">
                                        <label>Address line 1</label>
                                        <input type="text" name="address_1" id="address_1" required/>
                                    </div>

                                    <div class="col-xs-12 col-md-12">
                                        <label>Address line 2</label>
                                        <input type="text" name="address_2" id="address_2"/>
                                    </div>

                                    <div class="col-xs-12 col-md-12">
                                        <label>Address line 3</label>
                                        <input type="text" name="address_3" id="address_3"/>
                                    </div>
    
                                    <div class="col-xs-12 col-md-6">
                                        <label>1st Phone Number</label>
                                        <input type="text"  name="first_phone" id="first_phone" required/>
                                    </div>

                                    <div class="col-xs-12 col-md-6">
                                        <label>2nd Phone Number</label>
                                        <input type="text"  name="second_phone" id="second_phone"/>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <label>Password</label>
                                        <input type="password"  name="password" id="password" required/>
                                    </div>
                                    <div class="col-md-12">
                                        <label>Confirm Password</label>
                                        <input type="password"  name="password_confirmation" id="password_confirmation" required/>
                                    </div>

                                    @if($basic->google_recap == 1)
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                {!! app('captcha')->display() !!}
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-12 text-center">
                                    <input id="register" value="Register" type="submit">
                                </div>

                                <div class="text-center" style="text-transform: uppercase;">
                                    <br>
                                      <p style="color:#fff">Already have account? <a href="{{ route('login') }}">Login</a> </p>
                                    <br><br>
                                </div>
                            </form>
                    </div>
                 </div>
             </div>

        </div>
    </section>
    <!--login section end-->
<script src="{{ asset('assets/front/js/jquery.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        var input = document.getElementById('refid');
        if(input.value.length == 0){
            input.value = "root";
        }

        var pos = document.getElementById('position');
        if(pos.value.length == 0){
            pos.value = "Left";
        }
        $(":input").inputmask();
    });


</script>