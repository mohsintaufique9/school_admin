<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Favicon add-->
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/images/logo/icon.png') }}">
    <!--bootstrap Css-->
    <link href="{{ asset('assets/front/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--font-awesome Css-->
    <link href="{{ asset('assets/front/css/font-awesome.min.css') }}" rel="stylesheet">
    <!--Style Css-->
    <link href="{{ asset('assets/front/css/style.css') }}" rel="stylesheet">

    <!-- Mymain css -->
    <!--Responsive Css-->
    <link href="{{ asset('assets/front/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/mymain.css') }}" rel="stylesheet">
    
</head>
<style>
    select:required:invalid {
  color: gray;
}
option[value=""][disabled] {
  display: none;
}
option {
  background-color: #fff !important;
  color: black;
}
input{
  -webkit-user-select: initial !important;
  -khtml-user-select: initial !important;
  -moz-user-select: initial !important;
  -ms-user-select: initial !important;
  user-select: initial !important;
}
</style>


    <!--login section start-->
    <section class="bg">
        <div class="container">
            <div class="row">
                 <div class="head-login text-left">
                            <a href="{{url('/')}}"><img src="{{asset('assets/images/logo/logo.png')}}" style="max-height:80px;"></a>
                         </div>
                <div class="col-md-12 reg-login text-center">
                        <div class="login-form">
                                <div class="login-header text-center">
                                    <h4 style="color: #fff !important">Reset Password</h4>
                                </div> 
                            <br>
                            @if($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        {!!  $error !!}
                                    </div>
                                @endforeach
                            @endif
                            @if (session()->has('message'))
                                <div class="alert alert-{{ session()->get('type') }} alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            @if (session()->has('status'))
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ session()->get('status') }}
                                </div>
                            @endif

                            <form method="POST" action="{{ route('password.email') }}">
                                {{ csrf_field() }}
                                    <input type="text" name="email" id="email" required placeholder="Enter your Email"/>

                                    <input type="submit" value="Reset">

                            </form>
                </div>
            </div>
        </div>
    </section>

