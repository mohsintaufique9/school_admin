@extends('layouts.dashboard')
@section('style')
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption bold uppercase">
                                <strong><i class="fa fa-list"></i> School Statistics</strong>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                            </div>
                        </div>
                        <div class="portlet-body" style="overflow: hidden">

                            <a href="">
                                <div class="col-md-3">
                                    <div class="dashboard-stat blue">
                                        <div class="visual">
                                            <i class="fa fa-list"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span>{{ $total_users }}</span>
                                            </div>
                                            <div class="desc bold uppercase"> Total Users </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            
                            <a href="">
                                <div class="col-md-3">
                                    <div class="dashboard-stat green">
                                        <div class="visual">
                                            <i class="fa fa-list"></i>
                                        </div>
                                        <div class="details">
                                            <div class="number">
                                                <span>{{ $total_schools }}</span>
                                            </div>
                                            <div class="desc bold uppercase"> Total Schools </div>
                                        </div>
                                    </div>
                                </div>
                            </a>

                            <div class="col-md-3">
                                <div class="dashboard-stat red">
                                    <div class="visual">
                                        <i class="fa fa-list"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span>{{ $total_teachers }}</span>
                                        </div>
                                        <div class="desc bold uppercase"> Total Teachers</div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="dashboard-stat green">
                                    <div class="visual">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <div class="details">
                                        <div class="number">
                                            <span>{{ $total_students }}</span>
                                        </div>
                                        <div class="desc bold uppercase">Total Students</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
 document.getElementById("cbtn").onclick = function()
 {
   document.getElementById('rurl').select();
   document.execCommand('copy');
 }
</script>

@endsection
@section('scripts')

    <script src="{{ asset('assets/admin/js/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/admin/js/jquery.counterup.min.js') }}" type="text/javascript"></script>


@endsection