@extends('layouts.dashboard')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($schoolSite->name) ? $schoolSite->name : 'School Site' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('school_sites.school_site.destroy', $schoolSite->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('school_sites.school_site.index') }}" class="btn btn-primary" title="Show All School Site">
                        <span class="fa fa-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('school_sites.school_site.create') }}" class="btn btn-success" title="Create New School Site">
                        <span class="fa fa-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('school_sites.school_site.edit', $schoolSite->id ) }}" class="btn btn-primary" title="Edit School Site">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete School Site" onclick="return confirm(&quot;Click Ok to delete School Site.?&quot;)">
                        <span class="fa fa-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Name</dt>
            <dd>{{ $schoolSite->name }}</dd>
            <dt>Url</dt>
            <dd>{{ $schoolSite->url }}</dd>
            <dt>Created At</dt>
            <dd>{{ $schoolSite->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $schoolSite->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection