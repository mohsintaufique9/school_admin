@extends('layouts.dashboard')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="fa fa-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">School Sites</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('school_sites.school_site.create') }}" class="btn btn-success" title="Create New School Site">
                    <span class="fa fa-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($schoolSites) == 0)
            <div class="panel-body text-center">
                <h4>No School Sites Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Url</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($schoolSites as $schoolSite)
                        <tr>
                            <td>{{ $schoolSite->name }}</td>
                            <td>{{ $schoolSite->url }}</td>

                            <td>

                                <form method="POST" action="{!! route('school_sites.school_site.destroy', $schoolSite->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('school_sites.school_site.show', $schoolSite->id ) }}" class="btn btn-info" title="Show School Site">
                                            <span class="fa fa-eye" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('school_sites.school_site.edit', $schoolSite->id ) }}" class="btn btn-primary" title="Edit School Site">
                                            <span class="fa fa-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete School Site" onclick="return confirm(&quot;Click Ok to delete School Site.&quot;)">
                                            <span class="fa fa-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $schoolSites->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection