<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $site_title }} | {{ $page_title }}</title>
    <!--Favicon add-->
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/images/logo/icon.png') }}">
        <!-- Animation CSS -->
<link rel="stylesheet" href="assets/css/animate.css" >    
<!-- Latest Bootstrap min CSS -->
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="assets/css/ionicons.min.css">
<!--- owl carousel CSS-->
<link rel="stylesheet" href="assets/owlcarousel/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/owlcarousel/css/owl.theme.css">
<!-- Magnific Popup CSS -->
<link rel="stylesheet" href="assets/css/magnific-popup.css">
<!-- Style CSS -->
<link rel="stylesheet" href="assets/css/style1.css">
<link rel="stylesheet" href="assets/css/responsive1.css">
<link id="layoutstyle" rel="stylesheet" href="assets/color/theme.css">
<link rel="stylesheet" href="assets/owlcarousel/css/font-awesome.min.css">
</head>

<body data-spy="scroll" data-offset="110">
<!-- Start Pre-Loader-->

<div class="preloader">
    <div id="g-spinner" class="loading">
        <div class="circle c1"></div>
        <div class="circle c2"></div>
        <div class="circle c3"></div>
        <div class="circle c4"></div>
    </div>
</div>
<!-- End Preload -->
<!-- START HEADER -->
<header class="header_wrap fixed-top">
  <div class="container">
    <nav class="navbar navbar-expand-lg"> 
        <a class="navbar-brand page-scroll" href="{{url('/')}}">
            <img class="logo_light img-responsive" src="{{asset('assets/images/logo/logo.png')}}">
        </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="ion-android-menu"></span> </button>
      
      <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li><a class="nav-link page-scroll" href="#how_it_work">Products</a></li>
          <li><a class="nav-link page-scroll" href="#about">About</a></li>
          <li><a class="nav-link page-scroll" href="#token">Features</a></li>
          <!-- <li><a class="nav-link page-scroll" href="#team">Team</a></li> -->
          <!-- <li><a class="nav-link page-scroll" href="#partners">Partners</a></li> -->
          <!--<li><a class="nav-link page-scroll" href="#faq">FAQ</a></li>-->
          <!-- <li><a class="nav-link page-scroll" href="#contact">Contact</a></li> -->
        </ul>
        <ul class="navbar-nav nav_btn">
          @if(Auth::check())
            <li><a class="btn btn-default" href="{{ route('user-dashboard') }}">Virtual Office</a></li>
          @else
            <li><a class="btn btn-default" href="{{ route('login') }}">Login</a></li>
            <li><a class="btn btn-default" href="{{ route('register') }}">Register</a></li>
          @endif
        </ul>
      </div>
    </nav>
  </div>
</header>
<!-- START SECTION BANNER -->
<section id="home" class="banner_section section_gradiant banner_shape_effect">
  <div id="banner_bg_effect" class="banner_effect"></div>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-5 col-md-12 col-sm-12 order-lg-first text_md_center">
        <div class="banner_text">
          <h1>Earn Money Platform</h1>
          <p class="text-center" style="font-size: 18px;">Transform Your Life with Our Inovative Platform</p>
          <h4 class="text-center" style="color:yellow;margin-bottom:20px;">Don't Wait for a Miracle Make it Happen</h4>
          <div class="navbar-nav nav_btn">
            <a href="{{ route('register') }}" class="btn btn-default">Get Started</a> 
          </div>
          <p class="text-center">It's 100% FREE to Registar</p>
        </div>
      </div>
      <div class="col-lg-7 col-md-12 col-sm-12 order-first">
        <div class="banner_image_right res_md_mb_50 res_xs_mb_30"> 
            <img src="assets/images/banner_vector.png" alt="banner_vector"/> 
        </div>
        </div>
    </div>
  </div>
  <div class="section_wave" style="background-image:url('{{asset('assets/images/banner_wave.png')}}"></div>
</section>
<!-- END SECTION BANNER --> 
<!-- START SECTION HOW IT WORK -->
<section id="how_it_work" class="how_work" data-parallax="scroll" data-image-src="assets/images/how_it_work_bg.png">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-5 col-md-12 col-sm-12">
        <div class="title_dark">
          <h2>Products</h2>
          <p>Earn Money, a venture with diversification, a platform with an essence to change the stereotype of earning and a pioneer in resounding its financial resonance globally, has achieved many milestones and out for many more.Here’s a sneak peek of the features that we are currently working on and future services that will be released and implemented in the upcoming months.</p>
        </div>
        <a href="{{ route('register') }}" class="btn btn-default page-scroll">Get Started <i class="ion-ios-arrow-thin-right"></i></a> </div>
      <div class="col-lg-7 col-md-12 col-sm-12">
        <div class="work_box">
          <div class="box_inner"> <img src="assets/images/bot.png" style="max-height: 60px">
            <h4>Revolution</h4>
            <p>We're living in an incredible Crypto Era for which Bitcoin served as a pioneer and now it's grown tremendously into a global phenomenon.</p>
          </div>
        </div>
        <div class="work_box">
          <div class="box_inner"> <i class="ion-network"></i>
            <h4>Sharenodes</h4>
            <p>We take careful measures to ensure that your bitcoin is as safe as possible. Offline storage provides an 
              important security measure against theft or loss. </p>
          </div>
        </div>
        <div class="work_box">
          <div class="box_inner"> <img src="assets/images/Oil.png" style="max-height: 60px">
            <h4>Oil Trading</h4>
            <p>Blockchain works with exchange partners all around the world to make buying bitcoin in your wallet both a seamless and secure experience.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="shape shap1"></div>
</section>
<!-- END SECTION HOW IT WORK -->
<!-- START SECTION ABOUT US -->
<section id="about" class="section_gradiant">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-6 col-md-6 col-sm-12"> 
        <div class="res_sm_mb_30">
            <img src="assets/images/about_img.png" alt="aboutimg"/> 
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="title_light"> 
          <span>What is Earn Money</span>
          <h2>About Earn Money</h2>
          <p>A cryptocurrency is a digital or virtual currency designed to work as a medium of exchange. It uses cryptography to secure and verify transactions as well as to control the creation of new units of a particular cryptocurrency. </p>
          <p>Essentially, cryptocurrencies are limited entries in a database that no one can change unless specific conditions are fulfilled.</p>
        </div>
        <a href="https://www.youtube.com/watch?v=ZE2HxTmxfrI" class="btn btn-default video"><span class="ion-play"></span>Let's Start <i class="ion-ios-arrow-thin-right"></i></a> </div>
    </div>
    <div class="divider large_divider"></div>
    <div class="row text-center">
      <div class="col-lg-8 col-md-12 offset-lg-2">
        <div class="title_light"> 
            <span>Why Earn Money</span>
            <h2>Competitive Benefit</h2>
            <p>Competitive advantages are conditions that allow a company or country to produce a good or service at equal value but at a lower price or in a more desirable fashion</p>
        </div>
      </div>
    </div>
    <div class="row text-center small_space benefit_wrap">
      <div class="col-lg-2 col-md-4 col-6">
        <div class="benefit_box"> 
            <img src="assets/images/secure.png" alt="secure"/>
            <h6>Safe & Secure</h6>
        </div>
      </div>
      <div class="col-lg-2 col-md-4 col-6">
        <div class="benefit_box"> 
            <img src="assets/images/token.png" alt="token"/>
            <h6>Excerpt Tokens</h6>
        </div>
      </div>
      <div class="col-lg-2 col-md-4 col-6">
        <div class="benefit_box"> 
            <img src="assets/images/payment.png" alt="payment"/>
            <h6>Easy Payment</h6>
        </div>
      </div>
      <div class="col-lg-2 col-md-4 col-6">
        <div class="benefit_box"> 
            <img src="assets/images/case.png" alt="case"/>
            <h6>Weekly Cash Outs</h6>
        </div>
      </div>
      <div class="col-lg-2 col-md-4 col-6">
        <div class="benefit_box"> 
            <img src="assets/images/app.png" alt="app"/>
            <h6>Smart System</h6>
        </div>
      </div>
      <div class="col-lg-2 col-md-4 col-6">
        <div class="benefit_box"> 
            <img src="assets/images/dilution.png" alt="dilution"/>
            <h6>NO DILUTION</h6>
        </div>
      </div>
    </div>
  </div>
  <div class="rounded_shape rounded_shape1"></div>
  <div class="rounded_shape rounded_shape2"></div>
</section>
<!-- END SECTION ABOUT US --> 

<!-- START SECTION TOKEN -->
<section id="token" class="section_token">
  <div class="container">
    <div class="row text-center">
      <div class="col-md-12">
        <div class="title_dark">
          <h2>Why Choose Earn Money</h2>
        </div>
      </div>
    </div>
    <div class="row text-center align-items-center " id="img-s">
        <div class="col-md-6" >
            <div class="col-md-12">
                <img src="assets/images/legal.png">
            </div>
            <div class="col-md-12">
                <h5>We are a fully Legal Company</h5>
            </div>
            <div class="col-md-12">
                <p>Earn Money is a legal investment company that is incorporated and has its headquarters in UK.</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <img src="assets/images/finger.png">
            </div>
            <div class="col-md-12">
                <h5>Unique</h5>
            </div>
            <div class="col-md-12">
                <p>Earn Money has opened up the world of crypto finance (and the excellent profits that come with it) like no other company has before.</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <img src="assets/images/secure_c.png">
            </div>
            <div class="col-md-12">
                <h5>Secure Control Panel</h5>
            </div>
            <div class="col-md-12">
                <p>Our website utilizes secure control panel to help you monitor your account in real time.</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <img src="assets/images/clock.png">
            </div>
            <div class="col-md-12">
                <h5>ACCESSIBLE</h5>
            </div>
            <div class="col-md-12">
                <p>This opportunity truly is for everybody. The platform is simple and intuitive. And the autotrading feature drops the learning curve down to ZERO!</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <img src="assets/images/withdraw.png">
            </div>
            <div class="col-md-12">
                <h5>Easy and Quick Withdrawal</h5>
            </div>
            <div class="col-md-12">
                <p>Fast and easy withdrawal at anytime and every time you want.</p>
            </div>
        </div>
        <div class="col-md-6" >
            <div class="col-md-12">
                <img src="assets/images/alog.png">
            </div>
            <div class="col-md-12">
                <h5>VIRAL RESULTS</h5>
            </div>
            <div class="col-md-12">
                <p>How is that possible? Our unique algorithms don't rely on common indicators, allowing them to deal with extreme market fluctuations and avoid incurring severe losses.</p>
            </div>
        </div>
        
    </div>
  </div>
  <div class="shape shap1"></div>
  <div class="shape shap2"></div>
</section>
<!-- END SECTION TOKEN --> 
<!-- START SECTION TIMELINE & MOBILE APP -->
<!--<section id="road_map" class="section_gradiant">-->
<!--  <div class="container">-->
<!--    <div id="faq">-->
<!--        <div class="divider large_divider"></div>-->
<!--        <div class="row text-center">-->
<!--          <div class="col-lg-8 col-md-12 offset-lg-2">-->
<!--      <div class="title_dark"> -->
<!--        <span>FAQs</span>-->
<!--        <h2 style="color: #fff">Frequently Asked Questions</h2>-->
<!--        <p style="color: #fff">Frequently asked questions (FAQ) or Questions and Answers (Q&A), are listed questions and answers, all supposed to be commonly asked in some context</p>-->
<!--            </div>-->
<!--          </div>-->
<!--        </div>-->
<!--        <div class="row small_space">-->
<!--          <div class="col-lg-8 col-md-12 offset-lg-2">-->
<!--            <div id="accordion" class="faq_content">-->
<!--              <div class="card">-->
<!--                <div class="card-header" id="headingOne">-->
<!--                  <h6 class="mb-0"> <a data-toggle="collapse" class="gradient_box" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">What is cryptocurrency?</a> </h6>-->
<!--                </div>-->
<!--                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">-->
<!--                  <div class="card-body gradient_box"> Cryptocurrency is a form of payment that can be exchanged online for goods and services. Many companies have issued their own currencies, often called tokens, and these can be traded specifically for the good or service that the company provides. Think of them as you would arcade tokens or casino chips. You'll need to exchange real currency for the cryptocurrency to access the good or service. </div>-->
<!--                </div>-->
<!--              </div>-->
<!--              <div class="card">-->
<!--                <div class="card-header" id="headingTwo">-->
<!--                  <h6 class="mb-0"> <a class="collapsed gradient_box" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Which cryptocurrency is best to buy today?</a> </h6>-->
<!--                </div>-->
<!--                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">-->
<!--                  <div class="card-body gradient_box"> The best cryptocurrency to buy is one we are willing to hold onto even if it goes down. For example, I believe in Steem enough that I am willing to hold it even if it goes down 99% and would start buying more of it if the price dropped. </div>-->
<!--                </div>-->
<!--              </div>-->
<!--              <div class="card">-->
<!--                <div class="card-header" id="headingThree">-->
<!--                  <h6 class="mb-0"> <a class="collapsed gradient_box" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">How about day trading crypto?</a> </h6>-->
<!--                </div>-->
<!--                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">-->
<!--                  <div class="card-body gradient_box"> While profits are possible trading cryptocurrencies, so are losses. My first year involved me spending hundreds of hours trading Bitcoin with a result of losing over $5,000 with nothing to show for it. Simply trading digital currencies is very similar to gambling because no one really knows what is going to happen next although anyone can guess! While I was lucky to do nothing expect lose money when I started, the worst thing that can happen is to get lucky right away and get a big ego about what an amazing cryptocurrency trader we are. </div>-->
<!--                </div>-->
<!--              </div>-->
<!--              <div class="card">-->
<!--                <div class="card-header" id="headingFour">-->
<!--                  <h6 class="mb-0"> <a class="collapsed gradient_box" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">When to sell a cryptocurrency?</a> </h6>-->
<!--                </div>-->
<!--                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">-->
<!--                  <div class="card-body gradient_box"> Before Steem I was all in an another altcoin and really excited about it. When I first bought the price was low and made payments to me every week just for holding it. As I tried to participate in the community over the next several months, I was consistently met with a mix of excitement and hostility. When I began talking openly about this, the negative emotions won over in the community and in me. Originally I had invested and been happy to hold no matter what the price which quickly went up after I bought it. </div>-->
<!--                </div>-->
<!--              </div>-->
<!--            </div>-->
<!--          </div>-->
<!--        </div>-->
<!--    </div>-->
<!--  </div>-->
  
<!--  <div style="background-image:url('assets/images/wave.png');" class="section_wave2"></div>-->
<!--</section>-->
<!-- END SECTION TIMELINE & MOBILE APP --> 
<!-- START FOOTER SECTION --> 
<footer>
    <div class="bottom_footer">
      <div class="container">
          <div class="row">
              <div class="col-md-6">
                  <p class="copyright">Copyright &copy; 2018 All Rights Reserved.</p>
                </div>
                <!--<div class="col-md-6">-->
                <!--  <ul class="list_none footer_menu">-->
                <!--      <li><a href="#">Privacy Policy</a></li>-->
                <!--        <li><a href="#">Terms & Conditions</a></li>-->
                <!--    </ul>-->
                <!--</div>-->
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER SECTION --> 

<a href="#" class="scrollup btn-default" style="display: none;"><i class="ion-ios-arrow-up"></i></a>
<!-- Latest jQuery --> 
<script src="assets/js/jquery-1.12.4.min.js"></script> 
<!-- Latest compiled and minified Bootstrap --> 
<script src="assets/bootstrap/js/bootstrap.min.js"></script> 
<!-- owl-carousel min js  --> 
<script src="assets/owlcarousel/js/owl.carousel.min.js"></script> 
<!-- magnific-popup min js  --> 
<script src="assets/js/magnific-popup.min.js"></script> 
<!-- waypoints min js  --> 
<script src="assets/js/waypoints.min.js"></script> 
<!-- parallax js  --> 
<script src="assets/js/parallax.js"></script> 
<!-- countdown js  --> 
<script src="assets/js/jquery.countdown.min.js"></script> 
<!-- particles min js  --> 
<script src="assets/js/particles.min.js"></script>
<!-- scripts js --> 
<script src="assets/js/scripts.js"></script>
</body>
</html>